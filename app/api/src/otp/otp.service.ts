import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import config from '../config';
import axios from 'axios';
import { RequestOtpDto, VerifyOtpDto } from './dto/otp.dto';

const thaibulksmsApi = require('thaibulksms-api');
const options = config().otp;

const otp = thaibulksmsApi.otp(options);

@Injectable()
export class OtpService {
  constructor(){}
    async requestOTP(requestOtpDto: RequestOtpDto) {
        try {
            const { phoneNumber } = requestOtpDto;            
            const response = await otp.request(phoneNumber);
            return response.data;
        } catch (error) {
            throw error;
        }
    }

    async verifyOTP(verifyOtpDto: VerifyOtpDto) {
        try {
            const { token, otp } = verifyOtpDto;
            const url = "https://otp.thaibulksms.com/v2/otp/verify";
            const payload = {
                key: options.apiKey,
                secret: options.apiSecret,
                token,
                pin: otp
            };
            const response = await axios.post(url, payload, {
                headers: {
                    "Accept": 'application/json',
                    "Content-Type": 'application/x-www-form-urlencoded'
                }
            });
            return response.data;
        } catch (error) {
            if (error.response && error.response.data && error.response.data.errors && error.response.data.errors.length > 0) {
                const errorMessage = error.response.data.errors[0].message;
                throw new HttpException(errorMessage, HttpStatus.BAD_REQUEST);
            } else {
                throw new HttpException('Internal Server Error', HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }
}
