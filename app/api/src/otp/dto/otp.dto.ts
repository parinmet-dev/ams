import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, Matches } from 'class-validator';

export class RequestOtpDto {
  @ApiProperty({ 
    description: 'Phone number of the user in Thailand (starting with 0 followed by 8 or 9 digits)', 
    example: '0924434736' 
  })
  @IsNotEmpty()
  @IsString()
  @Matches(/^0[0-9]{8,9}$/)
  phoneNumber: string;
}

export class VerifyOtpDto {
  @ApiProperty({ 
    description: 'Token received after requesting OTP', 
    example: 'rXgym0DGdalekVDijte2ZYEMVxRA3KpP' 
  })
  @IsNotEmpty()
  @IsString()
  token: string;

  @ApiProperty({ 
    description: 'OTP (One-Time Password) received via SMS', 
    example: '1234' 
  })
  @IsNotEmpty()
  @IsString()
  otp: string;
}
