import { Controller, Post, Body, HttpStatus } from '@nestjs/common';
import { OtpService } from './otp.service';
import { RequestOtpDto , VerifyOtpDto} from './dto/otp.dto';
import { ApiBadRequestResponse, ApiBody, ApiInternalServerErrorResponse, ApiResponse, ApiTags } from '@nestjs/swagger';
import { ResponseBadRequest, ResponseInternalServerError } from '../utils/swagger-exception';
import { OtpRequestResponse, OtpVerifyResponse } from 'src/utils/swagger-exception/otp';

@Controller('otp')
@ApiTags('OTP')
export class OtpController {
  constructor(private readonly otpService: OtpService) {}

  @Post('/request-otp')
  @ApiBody({ type: RequestOtpDto })
  @ApiResponse({ status: HttpStatus.OK, type: OtpRequestResponse })
  @ApiBadRequestResponse({ type: ResponseBadRequest })
  @ApiInternalServerErrorResponse({ type: ResponseInternalServerError })
  async requestOTP(@Body() dto: RequestOtpDto): Promise<any> {
    const response = await this.otpService.requestOTP(dto);
    return { data: response, statusCode: HttpStatus.OK };
  }
  
  @Post('/verify-otp')
  @ApiBody({ type: VerifyOtpDto })
  @ApiResponse({ status: HttpStatus.OK, type: OtpVerifyResponse })
  @ApiBadRequestResponse({ type: ResponseBadRequest })
  @ApiInternalServerErrorResponse({ type: ResponseInternalServerError })
  async verifyOTP(@Body() dto: VerifyOtpDto): Promise<any> {
    const response = await this.otpService.verifyOTP(dto);
    return { data: response, statusCode: HttpStatus.OK };
  }
}
