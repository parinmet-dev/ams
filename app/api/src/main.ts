import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';
import { json, urlencoded } from 'express';
import { ValidationPipe } from '@nestjs/common';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';


async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const configService = app.get(ConfigService);
  if (process.env.NODE_ENV !== 'production') {
    const config = new DocumentBuilder()
      .setTitle('AMS API')
      .setDescription('API by Parinmet Dev.')
      .setVersion('1.0')
      .addBearerAuth()
      .addTag('AUTH')
      .addTag('OTP')
      .addTag('USER')
      .addServer('v1')
      .build();

    const document = SwaggerModule.createDocument(app, config);
    SwaggerModule.setup('api/swagger', app, document);
  }
  app.useGlobalPipes(new ValidationPipe());
  app.setGlobalPrefix(`api/v1`);
  app.use(json({ limit: '50mb' }));
  app.use(urlencoded({ extended: true, limit: '50mb' }));
  app.enableCors({
    origin: true,
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
    credentials: true,
  });

  await app.listen(configService.get<number>('port'), () => {
    console.log('Listening port: ' + configService.get<number>('port'));
  });  
}
bootstrap();
