export default () => ({
  environment: process.env.NODE_ENV,
  jwtSecret: process.env.JWT_SECRET || 'secret',
  port: process.env.APP_PORT ? parseInt(process.env.APP_PORT, 10) : 8089,
  mongoConfig: {
    host: process.env.MONGO_HOST || 'localhost',
    port: process.env.MONGO_PORT ? parseInt(process.env.MONGO_PORT, 10) : 27017,
    database: process.env.MONGO_DATABASE || 'nexus_db',
    username: process.env.MONGO_USERNAME || 'famworkmotion',
    password: process.env.MONGO_PASSWORD || 'Ic7QK6z9L7MFe1vh',
  },
  otp: {
    apiKey:  '1793849622449917', //process.env.OTP_KEY || '1793849622449917',
    apiSecret: '43be96a478a8406315f5ba607de6d10a' //process.env.OTP_SECRET || '43be96a478a8406315f5ba607de6d10a',
  },
  baseUploadPath: './public/uploads/',
});
