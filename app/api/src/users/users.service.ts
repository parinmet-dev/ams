import { BadRequestException, HttpException, HttpStatus, Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Users, UsersDocument } from './schema/user.schema';
import { IUser } from './interface/user.interface';
import { SoftDeleteModel } from 'soft-delete-plugin-mongoose';
import { ResponseWithText } from '../utils/component-dto/response.dto';
import * as argon2 from 'argon2';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(Users.name)
    private readonly usersModel: SoftDeleteModel<UsersDocument>,
  ) {}

  async register(createUserDto: CreateUserDto): Promise<IUser> {
    try {
      const createdUser = new this.usersModel(createUserDto);
      return await createdUser.save();
    } catch (error) {
      throw error;
    }
  }

  async findAll(): Promise<IUser[]> {
    try {
      const users = await this.usersModel.find({ isActive: true, isDeleted: false });
      return  users;
    } catch (error) {
      throw error;
    }
  }

  async findById(id: string): Promise<IUser> {
    try {      
      const user = await this.usersModel.findOne({ _id: id, isActive: true, isDeleted: false });            
      if (!user) {
        throw new NotFoundException('User not found');
      }

      return user;
    } catch (error) {
      throw error;
    }
  }

  async findByEmail(email: string): Promise<IUser> {
    try {
      return await this.usersModel.findOne({ email , isActive: true, isDeleted: false });
    } catch (error) {
      throw error;
    }
  }

  async update(id: string, updateUserDto: UpdateUserDto): Promise<IUser> {
    try {
      const user = await this.usersModel.findOne({ _id: id, isActive: true, isDeleted: false });            
      if (!user) {
        throw new NotFoundException('User not found');
      }
      
      return await this.usersModel.findByIdAndUpdate(id, updateUserDto, { new: true });
    } catch (error) {
      throw error;
    }
  }

  async remove(id: string): Promise<any> {
    try {
      const filter = { _id: id };
      const deleted = await this.usersModel.softDelete(filter);
      if (deleted.deleted === 0) {
        throw new BadRequestException('No Delete');
      }
      const response: ResponseWithText = {
        message: 'delete success',
        statusCode: HttpStatus.OK
      };
      return response;
    } catch (error) {
      throw error;
    }
  }

  async changePassword(id: string, newPassword: string) {
    try {
      const user = await this.usersModel.findOne({ _id: id, isDeleted: false });
      if (!user) {
        throw new HttpException('User not found', HttpStatus.NOT_FOUND);
      }
    
      user.password = await argon2.hash(newPassword);
      await user.save();
    
      const response: ResponseWithText = {
        message: 'Password changed success',
        statusCode: HttpStatus.OK
      };
      
      return response;
    } catch (error) {
      throw error;
    }
  }
}
