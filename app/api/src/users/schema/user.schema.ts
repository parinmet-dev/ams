import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes, Types } from 'mongoose';
import { softDeletePlugin } from 'soft-delete-plugin-mongoose';

export type UsersDocument = Users & Document;

@Schema({ timestamps: true })
export class Users {
  @Prop({
    required: true,
    unique: true,
    lowercase: true
  })
  email: string;

  @Prop({ required: false })
  portNumber: number;

  @Prop({ required: true })
  password: string;
  
  @Prop({ required: true })
  firstName: string;

  @Prop({ required: true })
  lastName: string;

  @Prop({ required: true })
  profileName: string

  @Prop({ required: false })
  title: string

  @Prop({ required: false })
  country: string

  @Prop({ required: false })
  province: string

  @Prop({ required: true })
  dateOfBirth: Date

  @Prop({ required: true })
  phoneNumber: string

  @Prop({ required: false })
  deposit: string

  @Prop({ default: true, required: true })
  isActive: boolean;

  @Prop({ required: false})
  role: string;

  @Prop({ required: false})
  refreshToken: string;

  @Prop({ type: SchemaTypes.ObjectId, ref: Users.name })
  userCreated: Types.ObjectId;

  @Prop({ type: SchemaTypes.ObjectId, ref: Users.name })
  userUpdated: Types.ObjectId;
}

export const UsersSchema = SchemaFactory.createForClass(Users).plugin(softDeletePlugin);
