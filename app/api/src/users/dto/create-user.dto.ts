import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsBoolean, IsString, Matches } from 'class-validator';

export class CreateUserDto {
  @ApiProperty({
    example: 'john.doe@example.com',
    description: 'The email address of the user',
    format: 'email',
  })
  @IsEmail()
  @IsString()
  email: string;

  @ApiProperty({
    example: 1,
    description: 'The port number of the email',
  })
  portNumber?: number;

  @ApiProperty({
    example: 'password123',
    description: 'The password of the user',
  })
  @IsNotEmpty()
  @IsString()
  password: string;

  @ApiProperty({
    example: 'John',
    description: 'The first name of the user',
  })
  @IsNotEmpty()
  @IsString()
  firstName: string;

  @ApiProperty({
    example: 'Doe',
    description: 'The last name of the user',
  })
  @IsNotEmpty()
  @IsString()
  lastName: string;

  @ApiProperty({
    example: 'Profile Name',
    description: 'The profile name of the user',
  })
  @IsNotEmpty()
  @IsString()
  profileName: string;

  @ApiProperty({
    example: 'Title',
    description: 'The title of the user',
  })
  title?: string;

  @ApiProperty({
    example: 'Country',
    description: 'The country of the user',
  })
  country?: string;

  @ApiProperty({
    example: 'Province',
    description: 'The province of the user',
  })
  province?: string;

  @ApiProperty({
    example: '2000-01-01',
    description: 'The date of birth of the user',
  })
  @IsNotEmpty()
  @IsString()
  dateOfBirth: Date;

  @ApiProperty({ 
    description: 'Phone number of the user in Thailand (starting with 0 followed by 8 or 9 digits)', 
    example: '0924434736' 
  })
  @IsNotEmpty()
  @IsString()
  @Matches(/^0[0-9]{8,9}$/)
  phoneNumber: string;

  @ApiProperty({
    example: null,
    description: 'The deposit of the user',
  })
  deposit?: string;

  @ApiProperty({
    example: null,
    description: 'The role of the user',
  })
  role: string;

  @ApiProperty({
    description: 'Refresh token for user authentication',
    example: null,
  })
  refreshToken?: string;
}
