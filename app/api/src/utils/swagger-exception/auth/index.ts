// auth-register-response.dto.ts

import { ApiProperty } from '@nestjs/swagger';

class DataTokens {
    @ApiProperty({
        description: 'Access token',
        example: 'your_access_token'
    })
    access_token: string;

    @ApiProperty({
        description: 'Refresh token',
        example: 'your_refresh_token'
    })
    refresh_token: string;
}

export class AuthRegisterResponse {
    @ApiProperty({
        description: 'HTTP status code',
        example: 201
    })
    statusCode: number;

    @ApiProperty({
        description: 'Message indicating the status',
        example: 'Created'
    })
    message: string;

    @ApiProperty({
        description: 'Data containing access and refresh tokens',
        type: () => DataTokens,
    })
    data: DataTokens;
}


export class AuthLoginResponse {
    @ApiProperty({
        description: 'HTTP status code',
        example: 200
    })
    statusCode: number;

    @ApiProperty({
        description: 'Message indicating the status',
        example: 'Success'
    })
    message: string;

    @ApiProperty({
        description: 'Data containing access and refresh tokens',
        type: DataTokens
    })
    data: DataTokens;
}

export class AuthRefreshTokenResponse {
    @ApiProperty({
        description: 'HTTP status code',
        example: 200
    })
    statusCode: number;

    @ApiProperty({
        description: 'Message indicating the status',
        example: 'Success'
    })
    message: string;

    @ApiProperty({
        description: 'Data containing access and refresh tokens',
        type: DataTokens
    })
    data: DataTokens;
}
