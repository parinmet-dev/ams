import { ApiProperty } from '@nestjs/swagger';

export class ApiSuccessResponse {
    @ApiProperty({ description: 'HTTP status code', example: 200 })
    statusCode: number;

    @ApiProperty({ description: 'Message indicating the status', example: 'Success' })
    message: string;
}

export class ResponseCreated extends ApiSuccessResponse {
    @ApiProperty({ description: 'HTTP status code', example: 201 })
    statusCode: number;

    @ApiProperty({ description: 'Message indicating the status', example: 'Created' })
    message: string;
}

export class ResponseOKWithData extends ApiSuccessResponse {
    @ApiProperty({ description: 'Data associated with the response', example: [] })
    data: any[];
}

export class ResponseNoContent extends ApiSuccessResponse {
    @ApiProperty({ description: 'Data associated with the response', example: [] })
    data: any[];
}

export class ResponseBadRequest extends ApiSuccessResponse {
    @ApiProperty({ description: 'HTTP status code', example: 400 })
    statusCode: number;

    @ApiProperty({ description: 'Message indicating the status', example: 'Bad Request' })
    message: string;
}

export class ResponseNotFound extends ApiSuccessResponse {
    @ApiProperty({ description: 'HTTP status code', example: 404 })
    statusCode: number;

    @ApiProperty({ description: 'Message indicating the status', example: 'Not Found' })
    message: string;
}

export class ResponseInternalServerError extends ApiSuccessResponse {
    @ApiProperty({ description: 'HTTP status code', example: 500 })
    statusCode: number;

    @ApiProperty({ description: 'Message indicating the status', example: 'Internal Server Error' })
    message: string;
}

  