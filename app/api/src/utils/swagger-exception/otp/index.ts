import { ApiProperty } from '@nestjs/swagger';

export class OtpRequestResponse {
    @ApiProperty({ description: 'Status of the operation', example: 'success' })
    status: string;
  
    @ApiProperty({ description: 'Generated token', example: 'xOj6G4yKdvXZo9BH0SEPR9nqazDQLAM5' })
    token: string;
  
    @ApiProperty({ description: 'HTTP status code', example: 200 })
    statusCode: number;
}
  

export class OtpVerifyResponse {
    @ApiProperty({ description: 'HTTP status code', example: 200 })
    statusCode: number;

    @ApiProperty({ description: 'Message indicating the status', example: 'Success' })
    message: string;
}