export class ResponseWithData {
  statusCode: number;
  message: string;
  data: any;
}

export class ResponseWithText {
  statusCode: number;
  message: string;
}