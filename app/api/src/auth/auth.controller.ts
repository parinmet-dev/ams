import { Body, Controller, Get, HttpStatus, Post, Req, UseGuards } from '@nestjs/common';
import { Request } from 'express';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { AuthService } from './auth.service';
import { AuthDto } from './dto/auth.dto';
import { RefreshTokenGuard } from './guards/refreshToken.guard';
import { AccessTokenGuard } from './guards/accessToken.guard';
import { ApiBadRequestResponse, ApiBearerAuth, ApiBody, ApiCreatedResponse, ApiInternalServerErrorResponse, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { ApiSuccessResponse, ResponseBadRequest, ResponseInternalServerError } from '../utils/swagger-exception';
import { AuthLoginResponse, AuthRefreshTokenResponse, AuthRegisterResponse } from '../utils/swagger-exception/auth';

@Controller('auth')
@ApiTags('AUTH')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('/register')
  @ApiBody({ type: CreateUserDto })
  @ApiCreatedResponse({ type: AuthRegisterResponse })
  @ApiBadRequestResponse({ type: ResponseBadRequest })
  @ApiInternalServerErrorResponse({ type: ResponseInternalServerError })
  async register(@Body() createUserDto: CreateUserDto) {
    return this.authService.register(createUserDto);
  }

  @Post('/login')
  @ApiBody({ type: AuthDto })
  @ApiResponse({ status: HttpStatus.OK, type: AuthLoginResponse })
  @ApiBadRequestResponse({ type: ResponseBadRequest })
  @ApiInternalServerErrorResponse({ type: ResponseInternalServerError })
  async login(@Body() data: AuthDto) {
    return this.authService.login(data);
  }

  @UseGuards(AccessTokenGuard)
  @ApiOperation({ summary: 'Logout using access token' })
  @ApiResponse({ status: HttpStatus.OK, type: ApiSuccessResponse })
  @ApiBadRequestResponse({ type: ResponseBadRequest })
  @ApiInternalServerErrorResponse({ type: ResponseInternalServerError })
  @ApiBearerAuth()
  @Get('logout')
  async logout(@Req() req: Request) {
    await this.authService.logout(req.user['sub']);
    return { statusCode: HttpStatus.OK, message: 'Logout success' };
  }

  @UseGuards(RefreshTokenGuard)
  @ApiOperation({ summary: 'Refresh access token using refresh token' })
  @ApiResponse({ status: HttpStatus.OK, type: AuthRefreshTokenResponse })
  @ApiBadRequestResponse({ type: ResponseBadRequest })
  @ApiInternalServerErrorResponse({ type: ResponseInternalServerError })
  @ApiBearerAuth()
  @Get('/refresh-token')
  async refreshTokens(@Req() req: Request) {        
    const userId = req.user['sub'];
    const refreshToken = req.user['refreshToken'];
    return this.authService.refreshToken(userId, refreshToken);
  }
}
