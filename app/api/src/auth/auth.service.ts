import { BadRequestException, ForbiddenException, HttpStatus, Injectable, InternalServerErrorException } from '@nestjs/common';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { UsersService } from 'src/users/users.service';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { AuthDto } from './dto/auth.dto';
import * as argon2 from 'argon2';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService,
  ) {}

  async register(createUserDto: CreateUserDto): Promise<any> {
    try {
      const { email, password } = createUserDto;

      const userExists = await this.usersService.findByEmail(email);
  
      if (userExists) {
        throw new BadRequestException('User already exists');
      }
  
      const hash = await this.hashData(password);
      const newUser = await this.usersService.register({
        ...createUserDto,
        password: hash,
      });
  
      const tokens = await this.generateTokens(newUser._id, newUser.email);
      await this.updateRefreshToken(newUser._id, tokens.refreshToken);
      return { statusCode: HttpStatus.CREATED , message: "Created" , data: tokens };
    } catch (error) {
      throw error;
    }
  }

  async login(authDto: AuthDto) {
    try {
      const { email, password } = authDto;

      const user = await this.usersService.findByEmail(email);
      if (!user) {
        throw new BadRequestException('User does not exist');
      }
  
      const passwordMatches = await argon2.verify(user.password, password);
      if (!passwordMatches) {
        throw new BadRequestException('Password is incorrect');
      }
  
      const tokens = await this.generateTokens(user._id, user.email);
      await this.updateRefreshToken(user._id, tokens.refreshToken);
      return { statusCode: HttpStatus.OK , message: "Success" , data: tokens };
    } catch (error) {
      throw error;
    }
  }

  async logout(userId: string) {
    try {
      return this.usersService.update(userId, { refreshToken: null });
    } catch (error) {
      throw error;
    }
    
  }

  async hashData(password: string): Promise<string> {
    try {
      return argon2.hash(password);
    } catch (error) {
      throw new InternalServerErrorException('Hashing failed');
    }
  }

  async updateRefreshToken(userId: string, refreshToken: string) {
    try {
      const hashedRefreshToken = await this.hashData(refreshToken);
      await this.usersService.update(userId, { refreshToken: hashedRefreshToken });
    } catch (error) {
      throw new InternalServerErrorException('Refresh token update failed');
    }
  }

  async generateTokens(userId: string, email: string): Promise<{ accessToken: string, refreshToken: string }> {
    try {
      const accessTokenExpiresIn = this.configService.get<string>('JWT_ACCESS_EXPIRATION_TIME'); // 15 minutes
      const refreshTokenExpiresIn = this.configService.get<string>('JWT_REFRESH_EXPIRATION_TIME'); // 7 days
  
      const [accessToken, refreshToken] = await Promise.all([
        this.jwtService.signAsync(
          { sub: userId, email },
          { secret: this.configService.get<string>('JWT_ACCESS_SECRET'), expiresIn: accessTokenExpiresIn || '15m' },
        ),
        this.jwtService.signAsync(
          { sub: userId, email },
          { secret: this.configService.get<string>('JWT_REFRESH_SECRET'), expiresIn: refreshTokenExpiresIn || '7d' },
        ),
      ]);
  
      return { accessToken, refreshToken };
    } catch (error) {
      throw new InternalServerErrorException('Token generation failed');
    }
  }


  async refreshToken(userId: string, refreshToken: string) {    
    try {
      const user = await this.usersService.findById(userId);
    
      if (!user || !user.refreshToken) {      
        throw new ForbiddenException('Access Denied');
      }
  
      const refreshTokenMatches = await argon2.verify(user.refreshToken, refreshToken);
      if (!refreshTokenMatches) {
        throw new ForbiddenException('Access Denied');
      }
  
      const tokens = await this.generateTokens(user.id, user.email);
  
      await this.updateRefreshToken(user.id, tokens.refreshToken);
  
      return { statusCode: HttpStatus.OK , message: "Success" , data: tokens };
    } catch (error) {
      throw error;
    }
  }
}
