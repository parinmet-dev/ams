import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { UsersService } from 'src/users/users.service';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(
    private reflector: Reflector,
    private usersService: UsersService,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const roles = this.reflector.get<string[]>('roles', context.getHandler());
    if (!roles) {
      return true;
    }

    const request = context.switchToHttp().getRequest();
    console.log(request.user)
    if (request?.user) {
      const { email } = request.user;
      const user = await this.usersService.findByEmail(email);
        if (!user) {
          return false;
        } else {
          const matchRoleUser = roles.includes(user.role);
          if (!matchRoleUser) {
            return false;
          } else {
            return true
          }
        }
    }
    return false;
  }
}
