import { Injectable } from '@nestjs/common';
import { CreateContactDto } from './dto/create-contact.dto';
import { UpdateContactDto } from './dto/update-contact.dto';
import * as soap from 'soap'; // lib การรับส่งข้อมูลแบบ xml

interface SoapArgs {
  username: string;
  password: string;
  TIN: string;
}

@Injectable()
export class ContactsService {
  private readonly url: string = 'https://rdws.rd.go.th/jsonRD/vatserviceRD3.asmx?wsdl';
  private readonly username: string = 'anonymous';
  private readonly password: string = 'anonymous';
  private readonly method: string = 'Service';

  async callSoapService(taxId: string): Promise<any> {
    try {
      const client: soap.Client = await soap.createClientAsync(this.url, {
        wsdl_options: { timeout: 1 * 60 * 1000 },
      });
      const args: SoapArgs = {
        username: this.username,
        password: this.password,
        TIN: taxId,
      };

      return new Promise<any>((resolve, reject) => {
        client[this.method](args, (err: any, result: any) => {
          if (err) {
            reject(err);
          } else {
            const r = result[this.method + 'Result'];
            resolve(r?.$value || r);
          }
        });
      });
    } catch (error) {
      throw new Error(`Error calling SOAP service: ${error.message}`);
    }
  }
}
