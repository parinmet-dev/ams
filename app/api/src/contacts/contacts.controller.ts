import { Controller, Get, Post, Body, Patch, Param, Delete, HttpException, HttpStatus } from '@nestjs/common';
import { ContactsService } from './contacts.service';
import { CreateContactDto } from './dto/create-contact.dto';
import { UpdateContactDto } from './dto/update-contact.dto';

@Controller('contacts')
export class ContactsController {
  constructor(private readonly contactsService: ContactsService) {}

  @Get(':taxId')
  async callSoapService(@Param('taxId') taxId: string) {    
      try {
          const data = await this.contactsService.callSoapService(taxId);
          return { data };
      } catch (error) {
          throw new HttpException('Internal Server Error', HttpStatus.INTERNAL_SERVER_ERROR);
      }
  }
}
