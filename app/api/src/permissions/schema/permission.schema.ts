import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes, Types } from 'mongoose';
import { softDeletePlugin } from 'soft-delete-plugin-mongoose';

export type PermissionsDocument = Permissions & Document;

@Schema({ timestamps: true })
export class Permissions {
  @Prop({ required: true })
  isActive: boolean;
  
  @Prop({ required: true })
  nameEn: string;

  @Prop({ required: true })
  nameTh: string;

  @Prop({ required: true })
  permissions: string[]; 

  @Prop({ type: SchemaTypes.ObjectId, ref: 'Users' })
  userCreated: Types.ObjectId;

  @Prop({ type: SchemaTypes.ObjectId, ref: 'Users' })
  userUpdated: Types.ObjectId;
}

export const PermissionsSchema = SchemaFactory.createForClass(Permissions).plugin(softDeletePlugin);
