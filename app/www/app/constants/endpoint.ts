import { API_BASE_URL } from ".";

export const ENDPOINT = { 
    LOGIN: `${API_BASE_URL}/demo`,
    REFRESH_TOKEN: `${API_BASE_URL}/refresh-token`
}