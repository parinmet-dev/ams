import { MenuProps } from "antd";
import { UserOutlined } from "@ant-design/icons";

export const accountItems: MenuProps["items"] = [
  {
    label: "กลับไปที่ PEAK เดิม",
    key: "1",
  },
  {
    label: "ข้อมูลส่วนตัว",
    key: "2",
    icon: <UserOutlined />,
  },
  {
    label: "ชำระค่าบริการ",
    key: "3",
  },
  {
    label: "ออกจากระบบ",
    key: "4",
  },
];

export const packageItems: MenuProps["items"] = [
    {
      label: "กลับไปที่ PEAK เดิม",
      key: "1",
    },
  ];