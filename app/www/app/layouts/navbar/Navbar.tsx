"use client";
import { useEffect, useState } from "react";
import { useRouter, usePathname } from "next/navigation";
import Image from "next/image";
import type { DropdownProps, MenuProps } from "antd";
import { Avatar, Dropdown, Menu, Space } from "antd";
import dashboardIcon from "../../../public/icons/dashboard.png";
import incomeIcon from "../../../public/icons/income.png";
import expensesIcon from "../../../public/icons/expenses.png";
import contactsIcon from "../../../public/icons/contacts.png";
import productIcon from "../../../public/icons/product.png";
import financeIcon from "../../../public/icons/finance.png";
import accountIcon from "../../../public/icons/account.png";
import folderIcon from "../../../public/icons/folder.png";
import settingsIcon from "../../../public/icons/settings.png";
import { accountItems, packageItems } from "./NavbarItems"
import {
  BellOutlined,
  DownOutlined,
  InfoCircleOutlined,
  ProjectOutlined,
  TableOutlined,
  UserOutlined,
} from "@ant-design/icons";

type MenuItem = Required<MenuProps>["items"][number];

const menuItems: MenuItem[] = [
  {
    label: "หน้าหลัก",
    key: "/page/dashboard",
    icon: (
      <Image src={dashboardIcon} alt="Dashboard Icon" width={24} height={24} />
    ),
  },
  {
    label: "รายรับ",
    key: "/page/income",
    icon: (
      <Image src={incomeIcon} alt="Dashboard Icon" width={24} height={24} />
    ),
  },
  {
    label: "รายจ่าย",
    key: "/page/expense",
    icon: (
      <Image src={expensesIcon} alt="Dashboard Icon" width={24} height={24} />
    ),
  },
  {
    label: "ผู้ติดต่อ",
    key: "/page/contacts",
    icon: (
      <Image src={contactsIcon} alt="Dashboard Icon" width={24} height={24} />
    ),
  },
  {
    label: "สินค้า",
    key: "/page/products",
    icon: (
      <Image src={productIcon} alt="Dashboard Icon" width={24} height={24} />
    ),
  },
  {
    label: "การเงิน",
    key: "/page/finance",
    icon: (
      <Image src={financeIcon} alt="Dashboard Icon" width={24} height={24} />
    ),
  },
  {
    label: "บัญชี",
    key: "/page/accounts",
    icon: (
      <Image src={accountIcon} alt="Dashboard Icon" width={24} height={24} />
    ),
  },
  {
    label: "คลังเอกสาร",
    key: "/page/documents",
    icon: (
      <Image src={folderIcon} alt="Dashboard Icon" width={24} height={24} />
    ),
    children: [
      {
        type: "group",
        label: "Item 1",
        children: [
          { label: "Option 1", key: "/page/documents:setting:1" },
          { label: "Option 2", key: "/page/documents:setting:2" },
        ],
      },
      {
        type: "group",
        label: "Item 2",
        children: [
          { label: "Option 3", key: "/page/documents:setting:3" },
          { label: "Option 4", key: "/page/documents:setting:4" },
        ],
      },
    ],
  },
  {
    label: "ตั้งค่า",
    key: "/page/settings",
    icon: (
      <Image src={settingsIcon} alt="Dashboard Icon" width={24} height={24} />
    ),
    children: [
      {
        type: "group",
        label: "Item 1",
        children: [
          { label: "Option 1", key: "/page/settings:setting:1" },
          { label: "Option 2", key: "/page/settings:setting:2" },
        ],
      },
      {
        type: "group",
        label: "Item 2",
        children: [
          { label: "Option 3", key: "/page/settings:setting:3" },
          { label: "Option 4", key: "/page/settings:setting:4" },
        ],
      },
    ],
  },
];

export const Navbar = () => {
  const router = useRouter();
  const pathname = usePathname();
  const [current, setCurrent] = useState("/dashboard");
  const [isMounted, setIsMounted] = useState(false);

  useEffect(() => {
    setIsMounted(true);
    setCurrent(pathname);
  }, []);

  const onClick: MenuProps["onClick"] = (e) => {
    console.log("click ", e);
    setCurrent(e.key);
    router.push(e.key);
  };

  const [open, setOpen] = useState(false);

  const handleMenuClick: MenuProps["onClick"] = (e) => {
    if (e.key === "3") {
      setOpen(false);
    }
  };

  const handleOpenChange: DropdownProps["onOpenChange"] = (nextOpen, info) => {
    if (info.source === "trigger" || nextOpen) {
      setOpen(nextOpen);
    }
  };

  const accountMenu: MenuProps["items"] = accountItems;
  const packageMenu: MenuProps["items"] = packageItems;
  return (
    <header className="relative flex flex-col items-center text-white">
      <div className="w-full flex justify-center py-[4rem] bg-gradient-to-r from-blue-600 to-blue-400">
        <div className="flex justify-between items-center w-[70%]">
          <div className="flex items-center cursor-pointer gap-2">
            <div>
              <img
                src="https://secure.peakaccount.com/img/PEAK-Logo.753ad4a6.png"
                alt=""
                width={120}
              />
            </div>
            <div
              className="line"
              style={{
                width: "1px",
                height: "30px",
                background: "#fff",
                marginLeft: "15px",
              }}
            ></div>
            <div>
              <Avatar size="large" icon={<ProjectOutlined />} />
            </div>
            <div>
              <div>
                <Dropdown
                  menu={{
                    items: packageMenu,
                    onClick: handleMenuClick,
                  }}
                  onOpenChange={handleOpenChange}
                  open={open}
                >
                  <a onClick={(e) => e.preventDefault()}>
                    <Space>
                      ปริญเมศร์ เนติสิทธิเกียรติ
                      <DownOutlined />
                    </Space>
                  </a>
                </Dropdown>
              </div>
              <div className="mt-1">Code: 5V84C5AE3</div>
            </div>
          </div>
          <div className="flex items-center gap-5 text-white cursor-pointer">
            <div>
              <BellOutlined style={{ fontSize: "24px" }} />
            </div>
            <div>
              <InfoCircleOutlined style={{ fontSize: "24px" }} />
            </div>
            <div>
              <TableOutlined style={{ fontSize: "24px" }} />
            </div>
            <div
              className="line"
              style={{
                width: "1px",
                height: "30px",
                background: "#fff",
                marginLeft: "15px",
              }}
            ></div>
            <div>
              <Dropdown
                menu={{
                  items: accountMenu,
                  onClick: handleMenuClick,
                }}
                onOpenChange={handleOpenChange}
                open={open}
              >
                <a onClick={(e) => e.preventDefault()}>
                  <Space>
                    ปริญเมศร์ เนติสิทธิเกียรติ
                    <DownOutlined />
                  </Space>
                </a>
              </Dropdown>
            </div>
            <div>
              <Avatar size="large" icon={<UserOutlined />} />
            </div>
          </div>
        </div>
      </div>
      <div className="fixed top-0 flex justify-center items-center bg-white text-white border rounded-lg shadow-xl w-[70%] px-4 py-7 mt-32 z-50">
        {menuItems.length && isMounted ? (
          <Menu
            onClick={onClick}
            selectedKeys={[current]}
            mode="horizontal"
            items={menuItems}
            className="w-full px-24"
            style={{ fontSize: "19px" }}
          />
        ) : null}
      </div>
    </header>
  );
};

export default Navbar;
