import Axios, { InternalAxiosRequestConfig, AxiosError, AxiosResponse, AxiosRequestHeaders } from 'axios';
import { ENDPOINT } from '../constants/endpoint';

const instance = Axios.create({});

interface ConfigWithRetryFlag extends InternalAxiosRequestConfig {
    _retry?: boolean;
}

instance.interceptors.request.use(
    async (config: InternalAxiosRequestConfig) => {
        const accessToken = localStorage?.getItem('accessToken');
        if (accessToken) {
            config.headers = {
                Authorization: `Bearer ${accessToken}`,
                Accept: 'application/json'
            } as AxiosRequestHeaders;
        }
        return config;
    },
    (error: any) => {
        return Promise.reject(error);
    }
);

instance.interceptors.response.use((response: AxiosResponse) => response,async (error: AxiosError) => {
        const originalConfig = error.config as ConfigWithRetryFlag;

        if (error?.response?.status !== 401) {
            return Promise.reject(error);
        } else if (error?.response?.status === 401 && !originalConfig._retry) {
            try {
                originalConfig._retry = true;

                if (!localStorage.getItem('refreshToken')) {
                    window.location.replace('/pages/login');
                } else {
                    const config: InternalAxiosRequestConfig = {
                        headers: {
                            Authorization: `Bearer ${localStorage.getItem('refreshToken')}`,
                            Accept: 'application/json'
                        } as AxiosRequestHeaders
                    };
                    const res = await Axios.get(ENDPOINT.REFRESH_TOKEN, config);
                    const { accessToken, refreshToken } = res.data;
                    localStorage?.setItem('accessToken', accessToken);
                    localStorage?.setItem('refreshToken', refreshToken);

                    return instance(originalConfig);
                }
            } catch (_error) {
                return Promise.reject(_error);
            }
        }
        return Promise.reject(error);
    }
);

export default instance;
