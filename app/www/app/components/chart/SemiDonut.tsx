"use client";
import React, { useState } from "react";
import dynamic from "next/dynamic";
import { ApexOptions } from "apexcharts";

// Dynamically import the ApexCharts component to prevent SSR issues
const ReactApexChart = dynamic(() => import("react-apexcharts"), {
  ssr: false,
});

const SemiChart: React.FC = () => {
  const [chartData] = useState<ApexOptions>({
    series: [14, 23, 21, 17, 15, 10, 12, 17, 21],
    // options: {
    //   chart: {
    //     type: 'polarArea',
    //   },
    //   stroke: {
    //     colors: ['#fff']
    //   },
    //   fill: {
    //     opacity: 0.8
    //   },
    //   responsive: [{
    //     breakpoint: 480,
    //     options: {
    //       chart: {
    //         width: 200
    //       },
    //       legend: {
    //         position: 'bottom'
    //       }
    //     }
    //   }]
    // },
  });

  return (
    <div id="chart">
      <ReactApexChart
        options={chartData}
        series={chartData.series}
        type="polarArea"
        height={350}
      />
    </div>
  );
};

export default SemiChart;
