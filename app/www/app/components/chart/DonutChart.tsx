"use client";
import React, { useState } from "react";
import dynamic from "next/dynamic";
import { ApexOptions } from "apexcharts";

// Dynamically import the ApexCharts component to prevent SSR issues
const ReactApexChart = dynamic(() => import("react-apexcharts"), {
  ssr: false,
});

const DonutChart: React.FC = () => {
  const [chartData] = useState<ApexOptions>({
    series: [44, 55, 41, 17, 15],
    // options: {
    //   chart: {
    //     type: "donut",
    //   },
    //   responsive: [
    //     {
    //       breakpoint: 480,
    //       options: {
    //         chart: {
    //           width: 200,
    //         },
    //         legend: {
    //           position: "bottom",
    //         },
    //       },
    //     },
    //   ],
    // },
  });

  return (
    <div id="chart">
      <ReactApexChart
        options={chartData}
        series={chartData.series}
        type="donut"
        height={350}
      />
    </div>
  );
};

export default DonutChart;
