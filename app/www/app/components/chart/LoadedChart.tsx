"use client";
import React, { useState } from 'react';
import dynamic from 'next/dynamic';
import { ApexOptions } from 'apexcharts';

// Dynamically import the ReactApexChart component to prevent SSR issues
const ReactApexChart = dynamic(() => import('react-apexcharts'), { ssr: false });

// Dummy data generation function (replace with your actual implementation)
const makeData = () => {
  return [
    { x: 'Q1', y: Math.floor(Math.random() * 100) },
    { x: 'Q2', y: Math.floor(Math.random() * 100) },
    { x: 'Q3', y: Math.floor(Math.random() * 100) },
    { x: 'Q4', y: Math.floor(Math.random() * 100) },
  ];
};

const colors = ['#008FFB', '#00E396', '#FEB019', '#FF4560', '#775DD0'];

const LoadedChart: React.FC = () => {
  const [series, setSeries] = useState<{ data: { x: string; y: number }[] }[]>([{ data: makeData() }]);
  const [options, setOptions] = useState<ApexOptions>({
    chart: {
      id: 'barYear',
      height: 400,
      width: '100%',
      type: 'bar',
      events: {
        dataPointSelection: function (e, chart, opts) {
          const quarterChartEl = document.querySelector("#chart-quarter") as HTMLElement;
          const yearChartEl = document.querySelector("#chart-year") as HTMLElement;

          if (opts.selectedDataPoints[0].length === 1) {
            if (quarterChartEl.classList.contains("active")) {
              updateQuarterChart(chart, 'barQuarter');
            } else {
              yearChartEl.classList.add("chart-quarter-activated");
              quarterChartEl.classList.add("active");
              updateQuarterChart(chart, 'barQuarter');
            }
          } else {
            updateQuarterChart(chart, 'barQuarter');
          }

          if (opts.selectedDataPoints[0].length === 0) {
            yearChartEl.classList.remove("chart-quarter-activated");
            quarterChartEl.classList.remove("active");
          }
        },
        updated: function (chart) {
          updateQuarterChart(chart, 'barQuarter');
        }
      }
    },
    plotOptions: {
      bar: {
        distributed: true,
        horizontal: true,
        barHeight: '75%',
        dataLabels: {
          position: 'bottom'
        }
      }
    },
    dataLabels: {
      enabled: true,
      textAnchor: 'start',
      style: {
        colors: ['#fff']
      },
      formatter: function (val, opt) {
        return opt.w.globals.labels[opt.dataPointIndex];
      },
      offsetX: 0,
      dropShadow: {
        enabled: true
      }
    },
    colors: colors,
    states: {
      normal: {
        filter: {
          type: 'desaturate'
        }
      },
      active: {
        allowMultipleDataPointsSelection: true,
        filter: {
          type: 'darken',
          value: 1
        }
      }
    },
    tooltip: {
      x: {
        show: false
      },
      y: {
        title: {
          formatter: function (val: any, opts: any) {
            return opts.w.globals.labels[opts.dataPointIndex];
          }
        }
      } as any
    },
    title: {
      text: 'Yearly Results',
      offsetX: 15
    },
    subtitle: {
      text: '(Click on bar to see details)',
      offsetX: 15
    },
    yaxis: {
      labels: {
        show: false
      }
    }
  });

  const [seriesQuarter, setSeriesQuarter] = useState<{ data: { x: string; y: number }[] }[]>([{ data: [] }]);
  const [optionsQuarter, setOptionsQuarter] = useState<ApexOptions>({
    chart: {
      id: 'barQuarter',
      height: 400,
      width: '100%',
      type: 'bar',
      stacked: true
    },
    plotOptions: {
      bar: {
        columnWidth: '50%',
        horizontal: false
      }
    },
    legend: {
      show: false
    },
    grid: {
      yaxis: {
        lines: {
          show: false
        }
      },
      xaxis: {
        lines: {
          show: true
        }
      }
    },
    yaxis: {
      labels: {
        show: false
      }
    },
    title: {
      text: 'Quarterly Results',
      offsetX: 10
    },
    tooltip: {
      x: {
        formatter: function (val, opts) {
          return opts.w.globals.seriesNames[opts.seriesIndex];
        }
      },
      y: {
        title: {
          formatter: function (val: any, opts: any) {
            return opts.w.globals.labels[opts.dataPointIndex];
          }
        }
      } as any
    }
  });

  const updateQuarterChart = (chart: any, id: string) => {
    const series = chart.w.globals.series;
    const labels = chart.w.globals.labels;
    const activeDataPointIndex = chart.w.globals.selectedDataPoints[0];

    if (activeDataPointIndex !== undefined) {
      const quarterData = makeData();
      setSeriesQuarter([{ data: quarterData }]);
    }
  };

  const changeData = () => {
    const newData = makeData();
    setSeries([{ data: newData }]);
  };

  return (
    <div>
      <div id="wrap">
        <div id="chart-year">
          <ReactApexChart options={options} series={series} type="bar" height={326} />
        </div>
        <select id="model" className="flat-select" onChange={changeData}>
          <option value="iphone5">iPhone 5</option>
          <option value="iphone6">iPhone 6</option>
          <option value="iphone7">iPhone 7</option>
        </select>
      </div>
    </div>
  );
};

export default LoadedChart;
