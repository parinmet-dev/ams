"use client";
import React, { useState } from "react";
import dynamic from "next/dynamic";
import { ApexOptions } from "apexcharts";

// Dynamically import the ApexCharts component to prevent SSR issues
const ReactApexChart = dynamic(() => import("react-apexcharts"), {
  ssr: false,
});

const PieChart: React.FC = () => {
  const [chartData] = useState<ApexOptions>({
    series: [44, 55, 13, 43, 22],
    // options: {
    //   chart: {
    //     width: 380,
    //     type: "pie",
    //   },
    //   labels: ["Team A", "Team B", "Team C", "Team D", "Team E"],
    //   responsive: [
    //     {
    //       breakpoint: 480,
    //       options: {
    //         chart: {
    //           width: 200,
    //         },
    //         legend: {
    //           position: "bottom",
    //         },
    //       },
    //     },
    //   ],
    // },
  });

  return (
    <div id="chart">
      <ReactApexChart
        options={chartData}
        series={chartData.series}
        type="pie"
        height={350}
      />
    </div>
  );
};

export default PieChart;
