declare module 'thai-id-validator' {
    function validateThaiID(id: string): boolean;
    export = validateThaiID;
  }
  