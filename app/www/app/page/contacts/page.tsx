"use client";
import React, { useState } from "react";
import { Input, Button, Space } from "antd";
import type { GetProp } from "antd";
import type { OTPProps } from "antd/es/input/OTP";
import validateThaiID from "thai-id-validator";

const Contacts: React.FC = () => {
  const [nationalId, setNationalId] = useState<string>("");
  const [isValidNationalId, setIsValidNationalId] = useState<boolean>(true);

  const onChange: GetProp<typeof Input.OTP, "onChange"> = (text) => {
    setNationalId(text);
    const result = validateThaiID(nationalId);
    setIsValidNationalId(result);
    console.log("Validation Result:", result);
  };

  const handleSearch = () => {
    const result = validateThaiID(nationalId);
    console.log("Validation Result:", result);
  };

  const sharedProps: OTPProps = {
    onChange,
  };

  return (
    <div>
      <div className="flex">
        <Input.OTP
          length={13}
          size="large"
          {...sharedProps}
          style={{ display: "flex", gap: "4px" }}
        />
        <Button
          type="primary"
          onClick={handleSearch}
          icon={<i className="fas fa-search" />}
        >
          ค้นหา
        </Button>
      </div>
      {!isValidNationalId ? (
        <p className="text-red-600">รูปแบบเลขทะเบียน 13 หลักไม่ถูกต้อง</p>
      ) : (
        ""
      )}
    </div>
  );
};

export default Contacts;
