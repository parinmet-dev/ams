"use client";
import React, { useState } from "react";
import {
  Layout,
  Menu,
  Button,
  Input,
  Table,
  Space,
  Card,
  Badge,
  Tabs,
} from "antd";
import {
  PlusOutlined,
  GroupOutlined,
  PrinterOutlined,
  ImportOutlined,
  AppstoreAddOutlined,
} from "@ant-design/icons";
import "./finance.css";

const { Content } = Layout;
const { Search } = Input;

const menuItems = [
  {
    key: "1",
    icon: <Badge status="default" />,
    label: "ทั้งหมด",
  },
  {
    key: "2",
    icon: <Badge status="success" />,
    label: "ลูกค้า",
  },
  {
    key: "3",
    icon: <Badge status="warning" />,
    label: "ผู้ขาย",
  },
  {
    key: "4",
    icon: <Badge status="error" />,
    label: "ปิดการใช้งาน",
  },
];

const data: any = [];
for (let i = 0; i < 25; i++) {
  data.push({
    id: i + 1,
    key: i + 1,
    name: `Edward King ${i}`,
    age: 32,
    group: `London, Park Lane no. ${i}`,
  });
}

const columns: any = [
  {
    title: "เลขที่",
    dataIndex: "id",
    key: "id",
    sorter: (a: any, b: any) => a.id - b.id,
    sortDirections: ["ascend", "descend"],
  },
  {
    title: "ชื่อ",
    dataIndex: "name",
    key: "name",
  },
  {
    title: "กลุ่ม",
    dataIndex: "group",
    key: "group",
  },
  {
    title: "คำสั่ง",
    dataIndex: "actions",
    key: "actions",
  },
];

const Finance: React.FC = () => {
  const [activeTab, setActiveTab] = useState("1");
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  const [loading, setLoading] = useState(false);

  const start = () => {
    setLoading(true);

    setTimeout(() => {
      setSelectedRowKeys([]);
      setLoading(false);
    }, 1000);
  };

  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    setSelectedRowKeys(newSelectedRowKeys);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };

  const hasSelected = selectedRowKeys.length > 0;

  const handleTabChange = (key: string) => {
    setLoading(true);

    setTimeout(() => {
      setActiveTab(key);
      setLoading(false);
    }, 2000);
  };

  return (
    <div className="w-full p-6 sm:p-10 space-y-6">
      <div className="flex flex-col space-y-6 md:space-y-0 md:flex-row justify-between">
        <div className="mr-6">
          <h2 className="text-gray-600 ml-0.5">ผู้ติดต่อ</h2>
        </div>
      </div>
      <Layout className="h-3/5">
        <Card style={{ borderRadius: "0px", marginRight: "0.1px" }}>
          <div className="flex justify-between items-center">
            <div className="flex justify-start items-center mt-3 mb-5 ml-2 text-lg font-semibold">
              กลุ่มมาตรฐาน
            </div>
            <div className="flex justify-end items-center">
              <Space>
                <Button type="primary" icon={<PlusOutlined />}>
                  เพิ่มผู้ติดต่อ
                </Button>
                <Button disabled={!hasSelected} icon={<GroupOutlined />}>
                  เพิ่มเข้ากลุ่ม
                </Button>
                <Button disabled={!hasSelected} icon={<ImportOutlined />}>
                  นำเข้าผู้ติดต่อ
                </Button>
                <Button
                  disabled={!hasSelected}
                  icon={<PrinterOutlined />}
                  onClick={start}
                  loading={loading}
                >
                  พิมพ์รายงาน
                </Button>
              </Space>
            </div>
          </div>
          <Tabs
            tabPosition="left"
            activeKey={activeTab}
            onChange={handleTabChange}
          >
            {menuItems.map((item) => (
              <Tabs.TabPane
                key={item.key}
                tab={
                  <>
                    <span className="flex items-center mr-12">
                      {item.icon}
                      <span className="ml-2">{item.label}</span>
                    </span>
                  </>
                }
              >
                {activeTab === item.key && (
                  <Content className="p-5 bg-white rounded-none">
                    <div className="flex justify-end items-center">
                      <Search
                        placeholder="ค้นหาด้วยชื่อ, เลขที่"
                        onSearch={(value) => console.log(value)}
                        className="w-48 ml-4"
                      />
                    </div>
                    <Badge count={selectedRowKeys.length} className="mb-1" />
                    <Table
                      rowSelection={rowSelection}
                      columns={columns}
                      dataSource={data}
                      loading={loading}
                      pagination={{
                        defaultPageSize: 5,
                        showSizeChanger: true,
                        pageSizeOptions: ["5", "10", "15", `${data.length}`],
                        showTotal: (total, range) =>
                          `Showing ${range[0]}-${range[1]} of ${total} items`,
                      }}
                    />
                  </Content>
                )}
              </Tabs.TabPane>
            ))}
          </Tabs>
          <div className="flex justify-start items-center mt-12 mb-5 ml-2 text-lg font-semibold gap-8">
            กลุ่มกำหนดเอง
            <Button type="primary" ghost icon={<AppstoreAddOutlined />} />
          </div>
        </Card>
      </Layout>
    </div>
  );
};

export default Finance;
