import { Card, Col, Row } from "antd";
import ColumnChart from "../../components/chart/ColumnChart";
import LoadedChart from "../../components/chart/LoadedChart";
import DonutChart from "../../components/chart/DonutChart";
import PieChart from "../../components/chart/PieChart";
import SemiChart from "../../components/chart/SemiDonut";
import LineChart from "../../components/chart/LineChart";

export const Income = () => {
  return (
    <div className="w-full p-6 sm:p-10 space-y-6">
      <div className="flex flex-col space-y-6 md:space-y-0 md:flex-row justify-between">
        <div className="mr-6">
          <h2 className="text-gray-600 ml-0.5">รายจ่าย</h2>
        </div>
      </div>
      <Row gutter={[16, 16]}>
        <Col xs={24} md={16}>
          <Card title="ใบแจ้งหนี้ที่รับชำระ รอรับชำระ และพ้นกำหนด">
            <ColumnChart />
          </Card>
        </Col>
        <Col xs={24} md={8}>
          <Card title="เอกสารที่ออก">
            <LoadedChart />
          </Card>
        </Col>
      </Row>
      <Row gutter={[16, 16]}>
        <Col xs={24} md={8}>
          <Card title="ขายอะไรดีสุด">
            {/* <DonutChart /> */}
          </Card>
        </Col>
        <Col xs={24} md={8}>
          <Card title="ขายใครได้มากที่สุด">
            {/* <PieChart /> */}
          </Card>
        </Col>
        <Col xs={24} md={8}>
          <Card title="รายได้อะไรมากที่สุด">
            {/* <SemiChart /> */}
          </Card>
        </Col>
      </Row>
      <Row gutter={[16, 16]}>
        <Col xs={24} sm={24} md={24}>
          <Card title="ลูกหนี้ที่ติดตาม">
            <LineChart />
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default Income;
