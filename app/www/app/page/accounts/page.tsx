"use client";
import React, { useState } from "react";
import {
  Layout,
  Menu,
  Button,
  Input,
  Table,
  Space,
  Card,
  Badge,
  Tabs,
} from "antd";
import {
  PlusOutlined,
  UserOutlined,
  GroupOutlined,
  PrinterOutlined,
  ImportOutlined,
} from "@ant-design/icons";

const { Content, Sider } = Layout;

const { Search } = Input;

const menuItems = [
  {
    key: "1",
    icon: <UserOutlined />,
    label: "ลูกค้า",
  },
  {
    key: "2",
    icon: <GroupOutlined />,
    label: "ผู้ขาย",
  },
  {
    key: "3",
    icon: <ImportOutlined />,
    label: "ปิดใช้งาน",
  },
];

// const data: any = []; // Replace with your data
const data: any = [];
for (let i = 0; i < 25; i++) {
  data.push({
    id: i + 1, // Assuming id is unique
    key: i + 1, // Use id or another unique identifier as key
    name: `Edward King ${i}`,
    age: 32,
    address: `London, Park Lane no. ${i}`,
  });
}

const columns = [
  {
    title: "เลขที่",
    dataIndex: "id",
    key: "id",
  },
  {
    title: "ชื่อ",
    dataIndex: "name",
    key: "name",
  },
  {
    title: "กลุ่ม",
    dataIndex: "group",
    key: "group",
  },
  {
    title: "คำสั่ง",
    dataIndex: "actions",
    key: "actions",
  },
];

const Accounts: React.FC = () => {
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  const [loading, setLoading] = useState(false);

  const start = () => {
    setLoading(true);

    setTimeout(() => {
      setSelectedRowKeys([]);
      setLoading(false);
    }, 1000);
  };

  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    console.log("selectedRowKeys changed: ", newSelectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };
  const hasSelected = selectedRowKeys.length > 0;

  return (
    <div className="w-full p-6 sm:p-10 space-y-6">
      <Layout className="h-3/5">
        <Layout>
          <Card
            style={{ borderRadius: "0px", marginRight: "0.1px" }}
            className="border-r-0"
          >
            {/* <Sider width={200} style={{ background: "#fff" }}>
              <div className="flex justify-start items-center my-3 ml-2 text-lg font-semibold">
                กลุ่มมาตรฐาน
              </div>

              <Menu
                mode="inline"
                defaultSelectedKeys={["1"]}
                style={{ height: "auto", borderRight: 0 }}
                items={menuItems}
              />
            </Sider> */}
            <Tabs
              tabPosition="left"
              items={new Array(3).fill(null).map((_, i) => {
                const id = String(i + 1);
                return {
                  label: `Tab ${id}`,
                  key: id,
                  children: `Content of Tab ${id}`,
                };
              })}
            />
          </Card>
          <Card className="w-full rounded-none">
            <div className="flex justify-end items-center">
              <Space>
                <Button type="primary" icon={<PlusOutlined />}>
                  เพิ่มผู้ติดต่อ
                </Button>
                <Button disabled={!hasSelected} icon={<GroupOutlined />}>
                  เพิ่มเข้ากลุ่ม
                </Button>
                <Button disabled={!hasSelected} icon={<ImportOutlined />}>
                  นำเข้าผู้ติดต่อ
                </Button>
                <Button
                  disabled={!hasSelected}
                  icon={<PrinterOutlined />}
                  onClick={start}
                  loading={loading}
                >
                  พิมพ์รายงาน
                </Button>
              </Space>
            </div>
            <Content className="p-6 bg-white rounded-none min-h-[280px]">
              <div className="flex justify-end items-center my-5">
                <Search
                  placeholder="ค้นหาด้วยชื่อ, เลขที่"
                  onSearch={(value) => console.log(value)}
                  style={{ width: 200, marginLeft: 16 }}
                />
              </div>
              <Badge count={selectedRowKeys.length} className="mb-1" />
              <Table
                rowSelection={rowSelection}
                columns={columns}
                dataSource={data}
              />
            </Content>
          </Card>
        </Layout>
      </Layout>
    </div>
  );
};

export default Accounts;
