import type { Metadata } from "next";
import { Sarabun } from "next/font/google";
import "./globals.css";
import { Footer } from "./layouts/footer/Footer";
import Navbar from "./layouts/navbar/Navbar";

const prompt = Sarabun({
  weight: ['300'],
  style: ['normal', 'italic'],
  subsets: ['thai'], 
  display: 'swap',
})

export const metadata: Metadata = {
  title: "การให้บริการ AMS E-SERVICE | การประปาส่วนภูมิภาค (Provincial Waterworks Authority)",
  description: "การให้บริการ AMS E-SERVICE | การประปาส่วนภูมิภาค (Provincial Waterworks Authority)",
  icons: {
    icon: '/brawser_icon.png', // /public path
  },
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <head>
        <title>{metadata.title as string}</title>
        <meta name="description" content={metadata.description as string} />
        <link rel="icon" href={'./brawser_icon.png'} />
      </head>
      <body className={prompt.className}>
        <Navbar />
          <div className="mt-16 p-5 w-[75%] flex items-center justify-center mx-auto">
            {children}
          </div>
        <Footer />
      </body>
    </html>
  );
}
